# Ember Skills Explorer
This is an Ember project demonstrating stregies to add rich animation to EmberJS applications.

## Installation Instructions (untested)
This project is built with brunch.

Install brunch then navigate to this project directory and type:

    brunch watch --server

Then head to the URL: `http://localhost:3333` to view the application in your browser.