App = Ember.Application.create();
App.ApplicationRoute = Ember.Route.extend({
    setupController: function() {
        this.controllerFor('skills').set('model', App.Skill.find());   
        this.controllerFor('jobs').set('model', App.Job.find());
    }
});

App.Router.map(function(){
	this.resource("jobs");
});

App.JobdetailRoute = Ember.Router.extend({
	
})

// App.SkillsGroupController = Ember.ArrayController.extend({
// 	itemController:'skill'
// })


App.SkillsController = Ember.ArrayController.extend({
    itemController:'skill',
	groupsList:function(){
		var groupsList = Ember.A();
		this.get('model').forEach(function(item){
			var groupName,
				that = this;
			groupName = item.get('group');
			
			if(!groupsList.findProperty('title',groupName)){
				groupsList.addObject(Ember.Object.create({
					title:groupName,
					slug:groupName.decamelize(),
					idslug:'#'+groupName.decamelize(),
					skills:that.filter(function(skillItem,index,enumerable){
						console.log('item = '+skillItem)
						console.log('checking '+skillItem.get('group')+' == '+groupName+' is '+ (skillItem.group == groupName))
						return (skillItem.get('group') == groupName);
					},this)
				}));
			};			
		},this);
		
		var groupsList = groupsList.sort(function(a,b){
			return (a.groupName > b.groupName);
		});
		
		return groupsList;
	}.property('@each.group')
});

App.SkillController = Ember.ObjectController.extend({
    needs:['jobs'],
    isSelected:false,
	
	jobCount:function(){
		return this.get('jobs').get('length');
	}.property('jobs.@each'),
	
    toggleSelected:function(){
        var jobsController = this.get('controllers').get('jobs');
        var isSelected = this.get('isSelected');
        if(!isSelected){
            jobsController.addFilterItem(this);
        }else{
            jobsController.removeFilterItem(this);
        }
        this.set('isSelected',!isSelected);
    }
});

App.SkillView = Ember.View.extend({
    templateName: 'skill',
	tagName: 'li',
	classNameBindings: ['controller.isSelected:active:']
});

App.JobsController = Ember.ArrayController.extend({
    itemController:'job',
    filterObjects:[],
    addFilterItem:function(item){
        if(!this.get('filterObjects').contains(item.get('title'))){
            var filterObjs = this.get('filterObjects');
            filterObjs.addObject(item.get('title'));
            this.set('filterObjects',filterObjs);
        }
    },
    removeFilterItem:function(item){
        if(this.get('filterObjects').contains(item.get('title'))){
            var filterObjs = this.get('filterObjects');
            filterObjs.removeObject(item.get('title'));
            this.set('filterObjects',filterObjs);
        }
    },
    filteredJobs:function(){
        var filterObjs = this.get('filterObjects');
 		return this.filter(function(job, index, enumerable){   
 			// display all if filter is empty
			if(filterObjs.length==0){
	            return true;
	        }
			// find all skills that match in filterObjs
            var found = false;   
            job.get('skills').forEach(function(jobSkill){
                found = found || filterObjs.contains(jobSkill.get('title'));
            });
            return found;
        });
    }.property('filterObjects.@each','@each')
});

App.ViewTransitionManager = Ember.Object.extend({
	
	//	vars
	viewClass:null,
	viewsToInsert:null,
	viewsToDestroy:null,
	
	//	methods
	init:function(){
		this._super();

		//	Run once the view properties have been set
		Ember.run.schedule('actions',this, function(){
			var viewClass,
				transitionManager=this;
			
			//	Convert string to class
			if(Ember.typeOf(this.viewClass)=='string'){
				//viewClass = eval(this.viewClass); //## DEPRECATED
				//trying not to use eval because AIR doesnt like it
				var parts,i,obj,len;
				parts = this.viewClass.split(".");
				for (i = 0, viewClass = window; i < parts.length; ++i) {
				    viewClass = viewClass[parts[i]];
				}
			}
				
			if(!viewClass){
				console.log('ERROR: no viewClass Set');
				return;
			}
				
			//	Crack open instance and force 
			//	 callbacks for creation and destruction.
			
			viewClass.reopen({
				init:function(){
					this._super();
					transitionManager.viewInit(this);
				},
				willDestroyElement:function(){
					transitionManager.viewDestroy(this);				
				}
			});			
			
		});
	},
	viewInit:function(view){
		this.registerView(view);
		Ember.run.scheduleOnce('afterRender', this, this.afterRender);
	},
	viewDestroy:function(view){
		this.deregisterView(view);
		Ember.run.scheduleOnce('afterRender', this, this.afterRender);
	},	
	deregisterView:function(view){
		var cont,
			view;
		
		cont = view.get('controller');
		
		if(this.viewsToInsert){
			
			view = this.viewsToInsert.get(cont);
			
			if(!this.viewsToDestroy)
				this.viewsToDestroy = Ember.Map.create();

			this.viewsToDestroy.set(cont, Ember.A([ 
				view, 
				this.getJQClone(view) 
			]));
			
			this.viewsToInsert.set(cont,null);
		}

	},
	getJQClone:function(view){

		var $holder	 = $('.vtm-anim-holder'),
			$orig 	 = view.$(),
			$clone 	 = $orig.clone(),
			position = $orig.position();

		if( $holder.length == 0 ){
			$holder = $('<div class="vtm-anim-holder"/>');
			$holder.css({
				position	: 'absolute',
				padding		: 0,
				margin		: 0,
				top			: 0,
				left		: 0,
				width		: '100%',
				height		: '100%',
				pointerEvents : 'none'
			}).appendTo($('body'));
		}
		
		return $clone.css({
			position	: 'absolute',
			listStyle	: 'none',
			top			: position.top+'px',
			left		: (position.left-parseInt($orig.find(">:first-child").css('paddingLeft')))+'px',
			width		: $orig.width()+'px',
			height		: $orig.height()+'px'
		}).appendTo($holder);
	},
	registerView:function(view){	
		
		var cont = view.get('controller');
		
		if( !this.viewsToInsert )
			this.viewsToInsert = Ember.Map.create();
		
		if( !this.viewsToInsert.get(cont) )
			this.viewsToInsert.set(cont,view);
			
	},
	afterRender:function(){
		
		//	Declare variables
		var itemsToRemove 		= Ember.A(),			
			itemsToCreate 		= Ember.A(),				
			itemsToTransition 	= Ember.A();
		
		// Run through all registrations
		if(!this.viewsToDestroy){
			return;
		}
		
		//	Cycle through the views being destroyed
		this.viewsToDestroy.forEach(function(cont,viewOldObj){
			
			var viewOld 		= viewOldObj[0],
				$viewOld 		= viewOldObj[1],
				viewNew 		= this.viewsToInsert.get(cont),
				$viewNew	 	= (!viewNew)?null:viewNew.$();
			
			if(!viewNew){			
				//	If the new view doesnt exist remove the old view
				itemsToRemove.addObject($viewOld);
			}else{						
				//	If the new view exists then transition from the old to it
				itemsToTransition.addObject(Ember.A([
						$viewOld, 
						$viewNew
				]));
			}
			
		},this);
		
		//	Cycle through the refreshed views
		this.viewsToInsert.forEach(function(cont, viewNew){
			
			var viewOldObj 	= this.viewsToDestroy.get(cont),
				$viewNew	= (!viewNew)?null:viewNew.$();
			
			//	if the old view doesnt exist and the 
			//	 new view exists add it to the create pile
			if( !viewOldObj && viewNew ){
				itemsToCreate.addObject( $viewNew );
			}
			
		},this);
		
		//	Clear all the deregistered views
		this.viewsToDestroy = Ember.Map.create();
		
		//	Run the remove animations
		itemsToRemove.forEach(function($item){
			this.removeAnimation($item);
		},this);

		//	Run the create animations		
		itemsToCreate.forEach(function($item){
			this.createAnimation($item);
		},this);

		//	Run the transition animations				
		itemsToTransition.forEach(function(itemArray){
			var $old = itemArray[0];
			var $new = itemArray[1];
			this.transitionAnimation($old,$new);
		},this);
		
	},
	removeAnimation : function($old){
		$old.remove();
	},
	createAnimation : function($new){
		
	},
	transitionAnimation : function($old,$new){
		$old.remove();
	}
	
});

App.jobTransitionManager = App.ViewTransitionManager.create({
	viewClass:'App.JobView',
	removeAnimation : function($jq){
		$jq.animate({opacity:0},500,function(){
			$jq.remove();
		});
	},
	createAnimation : function($jq){
		$jq.css({opacity:0});
		$jq.animate({opacity:1},500,function(){
		});
	},
	transitionAnimation : function($old,$new){
		var pos = $new.position();
		var width = $new.width();
		var height = $new.height();
		$new.css({opacity:0});
		$old.animate({
			top:pos.top+'px',
			left:(pos.left - parseInt($new.find(">:first-child").css('paddingLeft')))+'px',
			width:width+'px',
			height:height+'px',
		},500,function(){
			$new.css({opacity:1});
			$old.remove();
		});
	}
});

App.JobController = Ember.ObjectController.extend({
	isShowing:false,
	transition:null,
	positionDetails:null
});


App.JobView = Ember.View.extend({
    templateName: 'job',
	tagName: 'li',
	controllerName: 'App.JobController',
	classNameBindings: [':span4']
});

App.Store = DS.Store.extend({
	revision: 12,
	adapter:'DS.FixtureAdapter'
});

App.Job = DS.Model.extend({
    title: DS.attr('string'),
    location: DS.attr('string'),
    description: DS.attr('string'),	
	imageURL: DS.attr('string'),
	dateStarted: DS.attr('date'),
	dateEnded: DS.attr('date'),
	skills: DS.hasMany('App.Skill')
});

App.Skill = DS.Model.extend({
	title:DS.attr('string'),
	competence:DS.attr('number'),
	group:DS.attr('string'),
	jobs: DS.hasMany('App.Job')
});

App.Skill.FIXTURES = [
	{id:1, title:'jQuery', competence:95,jobs:[1,2,3,4,5,6],group:'Libraries'},
	{id:2, title:'Javascript', competence:95,jobs:[1,4],group:'Languages'},
	{id:3, title:'HTML5', competence:85,jobs:[1,4],group:'Languages'},
	{id:4, title:'Actionscript', competence:100,jobs:[2,3,5,6],group:'Languages'},
	{id:5, title:'Ruby on Rails', competence:40,jobs:[2,3,5,6],group:'Frameworks'},
	{id:6, title:'Ember JS', competence:60,jobs:[2,3,5,6],group:'Frameworks'},
	{id:7, title:'PHP',competence:80,jobs:[3,6],group:'Languages'},
	{id:8, title:'Codeigniter', competence:55,jobs:[3,6],group:'Frameworks'},
	{id:9, title:'MODx',competence:90,jobs:[3,6],group:'Frameworks'},
	{id:10, title:'Apache',competence:60,group:'Tools'},
	{id:11, title:'Linux',competence:70,group:'Tools'},
	{id:12, title:'Bash',competence:75,group:'Languages'},
	{id:13, title:'Online Video Solutions',competence:95,group:'Solutions'}
];

App.Job.FIXTURES = [
	{
		id:1, 
		title:'Job 1', 
		location:'Melbourne', 
		description: 'This is my job 1', 
		imageURL: 'http://dummyimage.com/300x225/000/999', 
		dateStarted:"2008-09-20", 
		dateEnded:"2009-09-20",
		skills:[1,2,3],
	},
	{
		id:2, 
		title:'Job 2', 
		location:'Berlin', 
		description: 'This is my job 2', 
		imageURL: 'http://dummyimage.com/300x225/FFF/444', 
		dateStarted:"2008-09-20", 
		dateEnded:"2009-09-20",
		skills:[1,4,5,6]
	},	
	{
		id:3, 
		title:'Job 3', 
		location:'New York', 
		description: 'This is my job 3', 
		imageURL: 'http://dummyimage.com/300x225/FFF/444', 
		dateStarted:"2008-09-20", 
		dateEnded:"2009-09-20",
		skills:[1,4,5,6,7,8,9]
	},
	{
		id:4, 
		title:'Job 1', 
		location:'Melbourne', 
		description: 'This is my job 1', 
		imageURL: 'http://dummyimage.com/300x225/000/999', 
		dateStarted:"2008-09-20", 
		dateEnded:"2009-09-20",
		skills:[1,2,3],
	},
	{
		id:5, 
		title:'Job 2', 
		location:'Berlin', 
		description: 'This is my job 2', 
		imageURL: 'http://dummyimage.com/300x225/FFF/444', 
		dateStarted:"2008-09-20", 
		dateEnded:"2009-09-20",
		skills:[1,4,5,6]
	},	
	{
		id:6, 
		title:'Job 3', 
		location:'New York', 
		description: 'This is my job 3', 
		imageURL: 'http://dummyimage.com/300x225/FFF/444', 
		dateStarted:"2008-09-20", 
		dateEnded:"2009-09-20",
		skills:[1,4,5,6,7,8,9]
	},
];
